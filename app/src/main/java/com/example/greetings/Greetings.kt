package com.example.greetings

fun greetOne(name:String?){
    if (name== null ){
        println(" Hello, my friend.")
    } else
    println(" Hello, $name")
}

fun greet(name:String?){
    when (name){
        null ->  println(" Hello, my friend.")
        name -> println(" Hello, $name")
        name.uppercase() -> println("HELLO ${name.uppercase()}")
    }
}
